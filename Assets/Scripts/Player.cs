﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public ShipStats Stats;
    public AudioSource LevelUp;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow)) //UI
        {
            transform.localPosition = new Vector3(3.35f, 0, -1);
            if (Input.GetKeyDown(KeyCode.Tab) && Stats.Scrap >= Stats.Cost_Upgrade)
            {
                Stats.Cost_UI /= 2;
                Stats.Scrap -= Stats.Cost_Upgrade;
                Stats.Cost_Upgrade *= 2;
                LevelUp.Play();
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow)) //Shields
        {
            transform.localPosition = new Vector3(-3.35f, 0, -1);
            if (Input.GetKey(KeyCode.Tab) && Stats.Charge > 0 && Stats.Shield < 100)
            {
                Stats.Shield += 0.1f;
                Stats.Charge -= 0.05f;
            }
        }
        else if (Input.GetKey(KeyCode.UpArrow)) //Engine
        {
            transform.localPosition = new Vector3(0, 3.35f, -1);
            if (Input.GetKeyDown(KeyCode.Tab) && Stats.Scrap >= Stats.Cost_Upgrade)
            {
                Stats.Cost_FuelEngines /= 2;
                Stats.Cost_Dampers /= 1.5f;
                Stats.Scrap -= Stats.Cost_Upgrade;
                Stats.Cost_Upgrade *= 2;
                LevelUp.Play();
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow)) //Steering
        {
            transform.localPosition = new Vector3(0, -3.35f, -1);
            if (Input.GetKeyDown(KeyCode.Tab) && Stats.Scrap >= Stats.Cost_Upgrade)
            {
                Stats.Cost_Steering /= 2;
                Stats.Scrap -= Stats.Cost_Upgrade;
                Stats.Cost_Upgrade *= 2;
                LevelUp.Play();
            }
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, -1);
        }
    }
}
