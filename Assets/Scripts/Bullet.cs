﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float BulletLifeTime;
    float PassedTime;
    public bool DisappearOnCollision;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if(PassedTime >= BulletLifeTime)
        {
            Destroy(gameObject);
        }else
        {
            PassedTime += Time.deltaTime;
        }	
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (DisappearOnCollision)
        {
            Destroy(gameObject);
        }
    }
}
