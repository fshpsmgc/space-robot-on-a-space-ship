﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalBossTrigger : MonoBehaviour {
    public float DetectionRadius;
    public LayerMask DetectionMask;
    public Canvas EndingCanvas;

    // Use this for initialization
    void Start () {
        EndingCanvas.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!Physics2D.OverlapCircle(transform.position, DetectionRadius, DetectionMask))
        {
            Time.timeScale = 0;
            EndingCanvas.enabled = true;
        }

        if(EndingCanvas.enabled && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}

