﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform Follow;
    public bool isFollow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isFollow)
        {
            transform.position = new Vector3(Follow.position.x, Follow.position.y, -10);
        }
        

	}
}
