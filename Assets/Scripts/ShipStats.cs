﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipStats : MonoBehaviour {
    public float Fuel;
    public float Charge;
    public float Scrap;
    public float Shield;
    public Color ShieldColor;

    public bool isUI;
    public bool isShield;
    public bool isSteering;  
    public bool isEngine;

    public float Cost_UI;
    public float Cost_Steering;
    public float Cost_Shield;
    public float Cost_Dampers;
    public float Cost_FuelEngines;
    public float Cost_Upgrade;
    public float Cost_Bullet;
    //UI
    public Canvas ShipUI;
    public Text Status;
    public Slider FuelSlider;
    public Slider ChargeSlider;
    public Slider ShieldSlider;

    public Canvas GameOverUI;
    public Text TitleText;
    public Text Stats;

    public int ScrapCollected;
    public int GreenThingiesKilled;
    public int RedThingiesKilled;
    public int AsteroidsDestroyed;

    bool isDestroyed;

    // Use this for initialization
    void Start () {
        FuelSlider.maxValue = Fuel;
        ChargeSlider.maxValue = Charge;
        ShieldSlider.maxValue = Shield;
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void Update () {
		if(Fuel < 0)
        {
            Fuel = 0;
        }

        if (Charge < 0)
        {
            Charge = 0;
        }

        if(Shield < 0)
        {
            Shield = 0;
        }

        if (isUI)
        {
            Charge -= Cost_UI;
        }

        if (isShield)
        {
            Charge -= Cost_Shield;
        }

        if (isSteering)
        {
            Charge -= Cost_Steering;
        }

        FuelSlider.value = Fuel;
        ChargeSlider.value = Charge;
        ShieldSlider.value = Shield;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            isUI = !isUI;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            isShield = !isShield;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            isSteering = !isSteering;
        }

        ShipUI.enabled = isUI;
        if (isShield)
        {
            GetComponent<SpriteRenderer>().color = ShieldColor;
        }else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }

        Status.text = "Scrap: " + Scrap + "\nUpgrade Cost: " + Cost_Upgrade + "\nUI: " + isUI + "(-" + Cost_UI + " charge)" + "\nShields: " + isShield + "(-" + Cost_Shield + " charge)" + "\nSteering: " + isSteering + "(-" + Cost_Steering + " charge)\n" + "Dampers cost (-" + Cost_Dampers + " charge)\n" + "Engine cost(-" + Cost_FuelEngines + " fuel)";

        if(isDestroyed)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Application.LoadLevel(0);
                Debug.Log("Wank");
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        if(Charge == 0 || Fuel == 0)
        {
            DestroyShip();
        }
    }

    public void DestroyShip()
    {
        
        TitleText.text = "You died!";
        if(Shield == 0)
        {
            TitleText.text += " Your shield ran out, so sort this out next time.";
        }

        if (!isShield)
        {
            TitleText.text += " Your hull can't take any hits, so make sure to turn it on every now and then.";
        }

        if(Fuel == 0)
        {
            TitleText.text += " You ran out of fuel and now floating through space. Don't believe the thing above, you're not really dead. Not yet. But it's quicker to just let you try again.";
        }

        if (Charge == 0)
        {
            TitleText.text += " You ran out of electrical charge. It's unfortunate that you're robot and you need that to power yourself up.";
        }
        Stats.text = "You destroyed:\n" + GreenThingiesKilled + " Green Thingies, you monster\n" + RedThingiesKilled + " Red Thingies\n" + AsteroidsDestroyed + " Asteroids\nAnd collected " + ScrapCollected + " Scrap. Use it to upgrade your ship next time!\n Anyway, press Space to try again or Escape to Quit." ;
        GameOverUI.enabled = true;
        isDestroyed = true;
        Time.timeScale = 0;
        Debug.Log("Destroyed");
    }
}
