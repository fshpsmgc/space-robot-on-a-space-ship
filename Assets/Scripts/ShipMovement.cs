﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(ShipStats))]
public class ShipMovement : MonoBehaviour {
    Rigidbody2D body;
    ShipStats stats;

    public float Acceleration;
    public float Rotation;
    Vector2 Move;

    public Transform RightGun;
    public Transform LeftGun;
    public Transform UpGun;
    public Transform DownGun;
    public GameObject Bullet;
    public float BulletSpeed;

    public AudioClip HitSound;
    public AudioClip ScrapSound;
    public AudioClip EngineSound;

    AudioSource audio;
    public AudioSource gunAudio;
    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
        stats = GetComponent<ShipStats>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        Move.x = Input.GetAxis("Horizontal");
        Move.y = Input.GetAxis("Vertical");

        //if(Move.x != 0)
        //{
        //    Accelerate(Move.x);
        //}

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Accelerate(1);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            Accelerate(-1);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            Dampers();
        }


        if (Move.x != 0)
        {
            Steer(Move.x);
        }else
        {
            body.AddTorque(-body.angularVelocity/25);
        }

        if (Input.GetKey(KeyCode.R))
        {
            Shoot();
        }else
        {
            gunAudio.Stop();
        }
    }

    void Accelerate(float Move)
    {
        if(stats.Fuel > 0)
        {
            body.AddForce(Acceleration * transform.right * Move);
            stats.Fuel -= stats.Cost_FuelEngines;
        }
        
    }

    void Steer(float Move)
    {
        if(stats.Charge > 0 && stats.isSteering)
        {
            //body.MoveRotation(body.rotation + (Rotation * Move));
            body.AddTorque(Rotation * Move);
            stats.Charge -= stats.Cost_Steering * 3;
        }
        
    }

    void Dampers()
    {
        if (stats.Charge > 0 && body.velocity != Vector2.zero)
        {
            body.AddForce(body.velocity * -1);
            stats.Charge -= stats.Cost_Dampers;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.tag != "Pickup")
        {
            audio.clip = HitSound;
            audio.Play();

            if(col.rigidbody != null)
            {
                col.rigidbody.AddForce(col.rigidbody.velocity * Random.Range(150, 400));
            }
            
            if(stats.Shield == 0 || !stats.isShield)
            {
                stats.DestroyShip();
            }else
            {
                stats.Shield -= 1;
            }
            
        }
    }

    void Shoot()
    {
        if(!gunAudio.isPlaying)
        {
            gunAudio.Play();
        }
        GameObject Temp;
        Temp = Instantiate(Bullet, RightGun.position, new Quaternion(0, 0, 0, 0));
        Temp.GetComponent<Rigidbody2D>().AddForce(transform.right * BulletSpeed);

        Temp = Instantiate(Bullet, LeftGun.position, new Quaternion(0, 0, 0, 0));
        Temp.GetComponent<Rigidbody2D>().AddForce(-transform.right * BulletSpeed);

        Temp = Instantiate(Bullet, UpGun.position, new Quaternion(0, 0, 0, 0));
        Temp.GetComponent<Rigidbody2D>().AddForce(transform.up * BulletSpeed);

        Temp = Instantiate(Bullet, DownGun.position, new Quaternion(0, 0, 0, 0));
        Temp.GetComponent<Rigidbody2D>().AddForce(-transform.up * BulletSpeed);

        stats.Charge -= stats.Cost_Bullet;
    }
}
