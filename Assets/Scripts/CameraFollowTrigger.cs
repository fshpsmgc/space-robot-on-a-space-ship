﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTrigger : MonoBehaviour {
    public CameraFollow cam;
    public float newSize;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            cam.isFollow = true;
            cam.GetComponent<Camera>().orthographicSize = newSize;
        }
        
    }
}
