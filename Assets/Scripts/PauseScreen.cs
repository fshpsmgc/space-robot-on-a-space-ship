﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScreen : MonoBehaviour {
    public bool Paused;
    public Canvas PauseCanvas;

	// Use this for initialization
	void Start () {
        PauseCanvas.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Paused = !Paused;
        }

        if (Paused)
        {
            Time.timeScale = 0;
            PauseCanvas.enabled = true;
        }else
        {
            Time.timeScale = 1;
            PauseCanvas.enabled = false;
        }
	}
}
