﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThingyAI : MonoBehaviour {
    public GameObject Player;
    public float DetectionRadius;
    public LayerMask DetectionMask;
    Rigidbody2D body;
    public float Speed;
    public int Health;

    public bool Follow;
    public bool ApplyForce;
    public bool isShooting;

    SpriteRenderer spr;

    public float ShootingSpeed;
    public Transform ShootingOrigin;
    public GameObject Bullet;
    public float BulletSpeed;

    float colourDetraction;
    float passedTime;

    AudioSource gunSound;
    public AudioSource hitSound;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        spr = GetComponent<SpriteRenderer>();
        if (Follow)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }

        if (ApplyForce)
        {
            body.AddForce(new Vector2(Random.Range(0, 15), Random.Range(0, 15)));
        }

        colourDetraction = 1 / (float)Health;

        if(isShooting)
        {
            gunSound = GetComponent<AudioSource>();
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Follow)
        {
            if (Physics2D.OverlapCircle(transform.position, DetectionRadius, DetectionMask))
            {
                body.velocity = transform.right * Speed;
                transform.right = Player.transform.position - transform.position;

                if (isShooting)
                {
                    if (passedTime >= ShootingSpeed)
                    {
                        Shoot();
                        passedTime = 0;
                    }
                    passedTime += Time.deltaTime;
                }
            }
        }



    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag != "Pickup")
        {
            if (col.rigidbody != null)
            {
                col.rigidbody.AddForce(col.rigidbody.velocity * UnityEngine.Random.Range(150, 400));
            }

            hitSound.Play();

            Health--;
            spr.color = new Color(spr.color.r, spr.color.g - colourDetraction, spr.color.b - colourDetraction);
            if(Health <= 0)
            {
                if (name.Contains("Red"))
                {
                    Player.GetComponent<ShipStats>().RedThingiesKilled++;
                }else
                {
                    Player.GetComponent<ShipStats>().GreenThingiesKilled++;
                }
                
                Destroy(gameObject);
            }
        }
    }

    void Shoot()
    {
        gunSound.Play();
        GameObject Temp;
        Temp = Instantiate(Bullet, ShootingOrigin.position, new Quaternion(0, 0, 0, 0));
        Temp.GetComponent<Rigidbody2D>().AddForce(transform.right * BulletSpeed);
    }
}
