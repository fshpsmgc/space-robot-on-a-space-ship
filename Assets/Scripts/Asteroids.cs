﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour {
    Rigidbody2D body;
    public int Health;
    SpriteRenderer spr;

    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
        spr = GetComponent<SpriteRenderer>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag != "Pickup")
        {

            Health--;
            spr.color = new Color(spr.color.r, spr.color.g - .2f, spr.color.b - .2f);
            if (Health <= 0)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<ShipStats>().AsteroidsDestroyed++;
                Destroy(gameObject);
            }
        }
    }


}
