﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            col.GetComponent<AudioSource>().clip = col.GetComponent<ShipMovement>().ScrapSound;
            col.GetComponent<AudioSource>().Play();
            col.GetComponent<ShipStats>().Scrap++;
            col.GetComponent<ShipStats>().ScrapCollected++;
            Destroy(gameObject);
        }
    }
}
